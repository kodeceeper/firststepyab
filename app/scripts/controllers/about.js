'use strict';

/**
 * @ngdoc function
 * @name firstStepYabApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the firstStepYabApp
 */
angular.module('firstStepYabApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
