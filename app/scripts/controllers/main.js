'use strict';

/**
 * @ngdoc function
 * @name firstStepYabApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the firstStepYabApp
 */
angular.module('firstStepYabApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
